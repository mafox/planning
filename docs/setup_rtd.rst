.. This should be a comment at the top of the document
   I wanted to set out the steps needed to produce your own ReadtheDocs page
   Filename: setup_rtd.rst
   Version: 0.1
   Author: M J Fox
   Creation date: 2021 05 22
   

==========================
Setting up for ReadtheDocs
==========================

Introduction
------------
The following steps should get you going

#. Set up a project on gitlab which will be used to create the documentation
#. Set up git on your computer to use sphinx, the connector between gitlab and readthedocs.
#. Set up an account on readthedocs.
#. Connect your gitlab account to readthedocs.
#. Import your project from gitlab to readthedocs.

After this any changes which are made in the gitlab repository will be automatically linked to readthedocs and the page(s) updated. All you have to do is add new content to your local git repository, commit the changes and then push the changes to gitlab.

My project name is planning. Obviously yours will be different so wherever you see planning replace it with the name of your project.


Set up git repository on gitlab.com
---------------------------------------



In gitlab.com create a new **public** project.

*The community version of rtd, readthedocs.org, does not support private projects.*

Set up git on your computer
---------------------------

On your computer clone the project on gitlab::

     git clone git@gitlab.com:mafox/planning.git
     
     
Note: this assumes you have git and python already installed on your computer.

Create a file  in the top level directory of the project (planning) called README.md and put a description of what the project is all about using ordinary Markdown.






I wanted to put my documentation in a docs folder so I created a subfolder docs under the planning folder::

    mkdir /Users/martin/planning/docs
    

Install and set up Sphinx on your computer
-------------------------------------------------

You need to have Sphinx installed::

    pip install sphinx
    
Go to the docs folder and run the quickstart::

    sphinx-quickstart

answering the few questions as appropriate.

You will also need the ReadTheDocs theme for Sphinx::

    pip install sphinx-rtd-theme
    
unless of course you want to use another theme. Read about themes on the `HTML theming page <https://www.sphinx-doc.org/en/master/usage/theming.html#themes>`_.

Themes can be found in the `Sphinx Themes Gallery <https://sphinx-themes.org>`_.

To set up Sphinx for use, make sure you are in the docs folder and check that there is a file called Makefile present in this folder.

Run::

	make HTML

and then add all the new files to git, commit the changes and push to gitlab::

	git add *
	git commit -m "Stuff for Sphinx"
	git push origin master

At this point if you open your project on gitlab in a browser you should see some folders and files.

.. image:: ../img/Gitlab_planning_project.png

To store image files create a folder img under planning.
Add the folder to git::

git add img

Any images you want to include should be copied into this folder.

It will be referenced like ../img/<imagefilename>

Create a file in the planning folder called .readthedocs.yaml and use the suggested syntax::

	# .readthedocs.yml
	# Read the Docs configuration file
	# See https://docs.readthedocs.io/en/stable/config-file/v2.html for details

	# Required
	version: 2

	# Build documentation in the docs/ directory with Sphinx
	sphinx:
	  configuration: docs/conf.py

	# Build documentation with MkDocs
	#mkdocs:
	#  configuration: mkdocs.yml

	# Optionally build your docs in additional formats such as PDF
	formats:
	  - pdf

	# Optionally set the version of Python and requirements required to build your docs
	#python:
	#  version: 3.7
	#  install:
	#    - requirements: docs/requirements.txt



To complete the sphinx set up create a file in the docs folder called config.py and edit the text below as appropriate::

	# Configuration file for the Sphinx documentation builder.
	#
	# This file only contains a selection of the most common options. For a full
	# list see the documentation:
	# https://www.sphinx-doc.org/en/master/usage/configuration.html

	# -- Path setup --------------------------------------------------------------

	# If extensions (or modules to document with autodoc) are in another directory,
	# add these directories to sys.path here. If the directory is relative to the
	# documentation root, use os.path.abspath to make it absolute, like shown here.
	#
	import os
	import sys
	sys.path.insert(0, os.path.abspath('.'))


	# -- Project information -----------------------------------------------------

	project = 'Planning'
	copyright = '2021, MJF'
	author = 'MJF'

	# The full version, including alpha/beta/rc tags
	release = '0.1'


	# -- General configuration ---------------------------------------------------

	# Add any Sphinx extension module names here, as strings. They can be
	# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
	# ones.
	extensions = [
		"sphinx_rtd_theme",
		"sphinx_rtd_theme",
		"sphinx_rtd_theme",
		"sphinx.ext.autosectionlabel",
	]

	# Add any paths that contain templates here, relative to this directory.
	templates_path = ['_templates']

	# List of patterns, relative to source directory, that match files and
	# directories to ignore when looking for source files.
	# This pattern also affects html_static_path and html_extra_path.
	exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


	# -- Options for HTML output -------------------------------------------------

	# The theme to use for HTML and HTML Help pages.  See the documentation for
	# a list of builtin themes.
	#
	html_theme = 'sphinx_rtd_theme'

	# Theme options are theme-specific and customize the look and feel of a theme
	# further.  For a list of options available for each theme, see the
	# documentation.
	#
	# html_theme_options = {}


	# Add any paths that contain custom static files (such as style sheets) here,
	# relative to this directory. They are copied after the builtin static files,
	# so a file named "default.css" will overwrite the builtin "default.css".
	html_static_path = ['_static']

	# Custom sidebar templates, must be a dictionary that maps document names
	# to template names.
	#
	# The default sidebars (for documents that don't match any pattern) are
	# defined by theme itself.  Builtin themes are using these templates by
	# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
	# 'searchbox.html']``.
	#
	# html_sidebars = {}


	# -- Options for HTMLHelp output ---------------------------------------------

	# Output file base name for HTML help builder.
	htmlhelp_basename = "planning"


Set up an index.rst file in the docs folder
-----------------------------------------------

Create a file index.rst in the docs folder. The content can look something like::

	.. Planning documentation master file, created by
	   sphinx-quickstart on Fri May 21 09:27:06 2021.
	   You can adapt this file completely to your liking, but it should at least
	   contain the root `toctree` directive.

	   Hello and Welcome to Martin's planning documentation!
	   =====================================================

	.. toctree::
	   :maxdepth: 3
	   :caption: Contents:

	   overview.rst
	   setup_rtd.rst


Add all new files (in my case I created empty files overview.rst, setup_rtd.rst) , commit and push to gitlab as usual.


To get the git files to readthedocs some setting up on the readthedocs website is needed.

Set up an account on readthedocs
--------------------------------

Go to `readthedocs.org <https://readthedocs.org>`_ and click Sign up if you do not already have an account on readthedocs.

Use the email address that you use to sign into gitlab.

Set up everything as needed. 

Connect your gitlab account to readthedocs
------------------------------------------

After signing in, click on Admin and then Connected Services.

Connect your gitlab account to readthedocs.

Import your project from gitlab
-------------------------------

If this works then when you click on your username you should be able to click on Import a Project and see all the repositories on gitlab to which you have access. You can filter the repositories by groups if necessary.

I imported mafox/planning project to readthedocs. When you are done importing a project and it reports that it is successfully connected click on the project (shown in the Projects window) and try to manually build a version. If you are successful you should see

.. image:: ../img/BuildCompleted.png

Click the link at the right "View docs" to see what it looks like so far.


Finishing up
------------

Additional rst files can be created in the docs folder and included in the index.rst file as shown above.

If you got this far and you have a page that looks something like

.. image:: ../img/rtd.png

then Congratulations!!!

For some quick start information on the reStructuredText markup language used by Sphinx go to
`reStructuredText Primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_.









