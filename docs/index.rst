.. Planning documentation master file, created by
   sphinx-quickstart on Fri May 21 09:27:06 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hello and Welcome to Martin's planning documentation!
=====================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   overview.rst
   setup_rtd.rst
   
   